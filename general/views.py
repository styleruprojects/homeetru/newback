import re

import requests
from django.contrib.auth import authenticate, login
from django.contrib.auth.hashers import make_password
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import get_template
from mozilla_django_oidc.auth import OIDCAuthenticationBackend
from mozilla_django_oidc.utils import absolutify
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from django.urls import reverse
from general.forms import AccountRegistrationForm, UserLoginForm, EmailForm, RestorePasswordForm
from django.core.mail import EmailMessage
import uuid
from general.models import Account
from homeetServer.settings import OIDC_OP_TOKEN_ENDPOINT, OIDC_RP_CLIENT_ID, OIDC_RP_CLIENT_SECRET


def to_up(request):
    return redirect('{}'.format(reverse('index')))


def index_student(request):
    if request.user.is_authenticated:
        return redirect('all-announcement')
    return registration(request, 'STUDENT')


def register_student(request):
    if request.method == 'POST' and 'iamrealtor' in request.POST:
        print("REALTORRRRR")
        return registration(request, 'REALTOR')
    return registration(request, 'STUDENT')


def index_realtor(request):
    if request.user.is_authenticated:
        return redirect('all-announcement')
    return registration(request, 'REALTOR')


def register_realtor(request):
    return registration(request, 'REALTOR')


def accept_email(request, code):
    try:
        account = Account.objects.get(email_accept_code=code)
    except Account.DoesNotExist:
        account = None

    form = AccountRegistrationForm()
    loginForm = UserLoginForm()
    if account is not None:
        account.email_accept_code = None
        account.save(update_fields=['email_accept_code'])
        form.fields["type"].initial = account.type

        if account.type == Account.Type.REALTOR:
            return render(request, 'general/index-realtor.html',
                          {'form': form, 'login_form': loginForm, 'restore_form': EmailForm()})

    return render(request, 'general/index-student.html',
                  {'form': form, 'login_form': loginForm, 'restore_form': EmailForm()})


def reset_password(request):
    restore_key = request.GET.get('restore_key', None)
    get_object_or_404(Account, restore_key=restore_key)
    loginForm = UserLoginForm()
    form = AccountRegistrationForm()
    form.fields["type"].initial = Account.Type.STUDENT
    restore_form = RestorePasswordForm()
    restore_form['restore_key'].initial = restore_key
    context = {
        'restore': True,
        'form': form,
        'login_form': loginForm,
        'restore_form': EmailForm(),
        'update_password_form': restore_form,
    }
    return render(request, 'general/index-student.html', context)


def restore_password_email(request):
    loginForm = UserLoginForm()
    form = AccountRegistrationForm()
    form.fields["type"].initial = Account.Type.STUDENT
    if request.method == 'POST':
        restore_form = EmailForm(request.POST)
        if restore_form.is_valid():
            email = restore_form.cleaned_data['email'].lower()
            if Account.objects.filter(email=email).exists():
                account = Account.objects.get(email=email)
                restore_key = uuid.uuid4().hex
                account.restore_key = restore_key
                account.save(update_fields=['restore_key'])
                href = 'http://' + str(request.get_host()) + '/reset-password?restore_key=' + restore_key
                send_restore_password_email(email, href)
    context = {
        'restore_form': EmailForm(),
        'form': form,
        'login_form': loginForm
    }
    return render(request, 'general/index-student.html', context)


def set_password(request):
    loginForm = UserLoginForm()
    form = AccountRegistrationForm()
    form.fields["type"].initial = Account.Type.STUDENT
    if request.method == 'POST':
        update_password_form = RestorePasswordForm(request.POST)
        if update_password_form.is_valid():
            account = Account.objects.get(restore_key=update_password_form.cleaned_data['restore_key'])
            password1 = update_password_form.cleaned_data['password1']
            password2 = update_password_form.cleaned_data['password2']
            if account:
                if check_password(password1, password2):
                    password = make_password(password1)
                    account.password = password
                    account.save(update_fields=['password'])
                    context = {
                        'restore_form': EmailForm(),
                        'form': form,
                        'login_form': loginForm
                    }
                    return render(request, 'general/index-student.html', context)

            context = {
                'restore': True,
                'restoreError': True,
                'form': form,
                'login_form': loginForm,
                'restore_form': EmailForm(),
                'update_password_form': update_password_form
            }
            return render(request, 'general/index-student.html', context)
    context = {
        'restore_form': EmailForm(),
        'form': form,
        'login_form': loginForm
    }
    return render(request, 'general/index-student.html', context)


def check_password(password1, password2):
    return password2 == password1 and len(password1) > 7 and len(re.findall('\\d', password1)) > 0 and \
           len(re.findall('[A-Z]', password1)) > 0 and len(re.findall('[a-z]', password1)) > 0 and \
           len(re.findall('[()[\\]{}|~!@#$%^&*_\\-+=;:\'",<>./?]', password1)) > 0


def registration(request, accountType):
    if request.method == 'POST':
        form = AccountRegistrationForm(request.POST)
        if form.is_valid():
            account = form.save(commit=False)
            email = form.cleaned_data['email'].lower()
            email_accept = uuid.uuid4().hex
            account.email_accept_code = email_accept
            account.email = email
            account.type = accountType
            if accountType == Account.Type.REALTOR:
                account.status = Account.Status.MODERATION
            account.save()

            href = 'http://' + str(request.get_host()) + '/activate/' + email_accept
            send_email(email, href)
            return redirect('index')
    else:
        form = AccountRegistrationForm()
    form.fields["type"].initial = accountType
    loginForm = UserLoginForm()
    return render(request, 'general/index-student.html',
                  {'form': form, 'login_form': loginForm, 'restore_form': EmailForm()})


def my_login(request):
    if request.POST:
        loginForm = UserLoginForm(request, data=request.POST)
        if loginForm.is_valid():
            user = authenticate(username=request.POST['username'].lower(), password=request.POST['password'])
            if user:
                if user.email_accept_code is None:
                    login(request, user)
                    return redirect('all-announcement')
    else:
        loginForm = UserLoginForm()
    form = AccountRegistrationForm()
    return render(request, 'general/index-student.html', {'form': form, 'login_form': loginForm, 'error': True, 'restore_form': EmailForm()})


def my_login_realtor(request):
    if request.POST:
        loginForm = UserLoginForm(request, data=request.POST)
        if loginForm.is_valid():
            user = authenticate(username=request.POST['username'].lower(), password=request.POST['password'])
            if user:
                if user.type == 'REALTOR' and user.email_accept_code is None:
                    login(request, user)
                    return redirect('all-announcement')

    loginForm = UserLoginForm()
    form = AccountRegistrationForm()
    return render(request, 'general/index-realtor.html', {'form': form, 'login_form': loginForm, 'error': True, 'restore_form': EmailForm()})


def token_auth(request):
    if 'code' in request.GET:
        backend = OIDCAuthenticationBackend()

        reverse_url = backend.get_settings('OIDC_AUTHENTICATION_CALLBACK_URL', 'oidc_authentication_callback')

        token_payload = {
            'client_id': OIDC_RP_CLIENT_ID,
            'client_secret': OIDC_RP_CLIENT_SECRET,
            'grant_type': 'authorization_code',
            'code': request.GET['code'],
            'redirect_uri': absolutify(
                request,
                reverse(reverse_url)
            ),
        }

        # Get the token
        token_info = backend.get_token(token_payload)
        id_token = token_info.get('id_token')
        access_token = token_info.get('access_token')

        user_info = backend.get_userinfo(access_token, id_token, None)
        email = user_info.get('email')
        username = email[:(email.index('@') + 1)]
        if Account.objects.filter(email__startswith=username).exists():
            account = Account.objects.get(email__startswith=username)
            login(request, account)
        else:
            account = Account(email=email, type=Account.Type.STUDENT, status=Account.Status.ACTIVE)
            account.save()
            login(request, account)

    return redirect('all-announcement')


@api_view(['POST'])
def save_photo(request):
    account = get_object_or_404(klass=Account, pk=request.user.pk)
    photo = request.FILES.get("photo")
    account.photo.save(photo.name, photo)
    account.save()
    return Response(status=status.HTTP_200_OK)


def send_email(email, href):
    html = get_template('general/email_template.html')
    d = {'href': href}
    html_content = html.render(d)
    print(email)
    subject, from_email, to = 'Регистрация в Homeet', 'homeet@hse.ru', email
    msg = EmailMessage(subject, html_content, from_email, [to])
    msg.content_subtype = "html"
    msg.send()


def send_restore_password_email(email, href):
    html = get_template('general/restore_password_email_template.html')
    d = {'href': href}
    html_content = html.render(d)
    subject, from_email, to = 'Восстановление пароля в Homeet', 'homeet@hse.ru', email
    msg = EmailMessage(subject, html_content, from_email, [to])
    msg.content_subtype = "html"
    msg.send()
