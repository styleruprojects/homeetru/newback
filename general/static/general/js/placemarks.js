ymaps.ready(init);

function init() {
    var myMap = new ymaps.Map('map', {
        center: [55.751574, 37.573856],
        zoom: 9,
        controls: []
    }, {
        searchControlProvider: 'yandex#search'
    });

    var zoomControl = new ymaps.control.ZoomControl({
        options: {
            size: "small"
        }
    });
    myMap.controls.add(zoomControl);
    myMap.behaviors.disable('drag');

    const announcement_type = document.getElementsByClassName('announcement-type')[0].id;

    $.ajax({
        type:'GET',
        url: "https://homeet.hse.ru/announcement/all/coordinates?type=" + announcement_type,
        success: function(response){
            for (const i in response) {
                const announcement = response[i];
                var placemark = new ymaps.Placemark([announcement.longitude, announcement.latitude], {
                    balloonContentHeader: '<span class="description">' + announcement.title + '</span>',
                    balloonContentBody: '<img src="' + announcement.image + '" height="150" width="200"> <br/> ' +
                        '<b>Цена: </b> <br/>' + announcement.price,
                    balloonContentFooter: '<a href="' + announcement.href + '">Посмотреть</a>',
                    hintContent: announcement.title
                });
                // Добавим метку на карту.
                myMap.geoObjects.add(placemark);
            }
        },
        error: function(error){
            console.log('error', error)
        },
        cache: false,
        contentType: false,
        processData: false,
    })
}