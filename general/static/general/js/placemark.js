ymaps.ready(init);

function init() {
    const longitude = document.getElementById('longitude')
    const latitude = document.getElementById('latitude')
    var coordinate = [longitude.value, latitude.value];

    var myMap = new ymaps.Map('map', {
        center: coordinate,
        zoom: 15,
        controls: []
    }, {
        searchControlProvider: 'yandex#search'
    });

    var zoomControl = new ymaps.control.ZoomControl({
        options: {
            size: "small"
        }
    });
    myMap.controls.add(zoomControl);
    myMap.behaviors.disable('drag');

    var placemark = new ymaps.Placemark(coordinate);
    myMap.geoObjects.add(placemark);
}