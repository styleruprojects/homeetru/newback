//-----JS for Price Range slider-----

$(function() {
	let minValue = $( "#id_min_price" ).val() == 0 ? 0 : $( "#id_min_price" ).val();
	let maxValue = $( "#id_max_price" ).val() == 0 ? 100000 : $( "#id_max_price" ).val();
	console.log(minValue, maxValue)
	$( "#slider-range" ).slider({
	  range: true,
	  min: 0,
	  max: 100000,
	  values: [ minValue, maxValue ],
	  slide: function( event, ui ) {
		$( "#amount" ).val( "₽" + ui.values[ 0 ] + " - ₽" + ui.values[ 1 ] );
		$( "#id_min_price" ).val( ui.values[ 0 ] );
		$( "#id_max_price" ).val( ui.values[ 1 ] );
	  }
	});
	$( "#amount" ).val( "₽" + $( "#slider-range" ).slider( "values", 0 ) +
	  " - ₽" + $( "#slider-range" ).slider( "values", 1 ) );
	$( "#id_min_price" ).val( $( "#slider-range" ).slider( "values", 0 ) );
	$( "#id_max_price" ).val( $( "#slider-range" ).slider( "values", 1 ) );
});