console.log('hello world')
const imageBox = document.getElementById('image-box')
const input = document.getElementById('images')


input.addEventListener('change', ()=>{
    const images = input.files
    var html = ""
    for (var i = 0; i < images.length; i++) {
        const url = URL.createObjectURL(images[i])
        html += `<img src="${url}" id="image" width="300px">`
    }
    console.log(html)

    imageBox.innerHTML = html

})