ymaps.ready(init);

function init() {
    var myMap = new ymaps.Map('map', {
        center: [55.751574, 37.573856],
        zoom: 9,
        controls: []
    }, {
        searchControlProvider: 'yandex#search'
    });

    var zoomControl = new ymaps.control.ZoomControl({
        options: {
            size: "small"
        }
    });
    myMap.controls.add(zoomControl);
    myMap.behaviors.disable('drag');

    const input = document.getElementById('suggest')
    const longitude = document.getElementById('longitude')
    const latitude = document.getElementById('latitude')
    var suggestView1 = new ymaps.SuggestView('suggest');
    input.addEventListener('blur', function() {
        var myGeocoder = ymaps.geocode(input.value);
        myMap.geoObjects.removeAll();
        myGeocoder.then(
            function (res) {
                var coordinate = res.geoObjects.get(0).geometry.getCoordinates();
                var placemark = new ymaps.Placemark(coordinate);
                myMap.geoObjects.add(placemark);
                longitude.value = coordinate[0]
                latitude.value = coordinate[1]
                myMap.setCenter(coordinate, 15);
            },
            function (err) {
                console.log('Ошибка', err);
            }
        );
    });
}