from django.core.mail import EmailMessage
from django.db import models
from django.contrib.auth.models import AbstractUser
from django.template.loader import get_template


def get_profile_image(self, name):
    return f'profile_images/{self.pk}/{name}'


def get_default_avatar():
    return "default/default_avatar.png"


class Account(AbstractUser):
    email = models.EmailField(unique=True)
    username = models.CharField(max_length=255, unique=False)
    photo = models.ImageField(max_length=255, upload_to=get_profile_image, null=True, blank=True,
                              default=get_default_avatar)
    phone = models.CharField(max_length=255)
    about = models.TextField()
    email_accept_code = models.UUIDField(default=None, null=True, blank=True)

    restore_key = models.CharField(max_length=255, null=True, blank=True)
    course = models.IntegerField(null=True, blank=True)
    faculty = models.CharField(max_length=255, null=True, blank=True)
    realtor_type = models.CharField(max_length=100, null=True, blank=True)
    company_name = models.CharField(max_length=255, null=True, blank=True)
    telegram = models.CharField(max_length=255, null=True, blank=True)

    class Status(models.TextChoices):
        ACTIVE = "Активен"
        MODERATION = "На модерации"
        DISABLED = "Не активен"
    status = models.CharField(max_length=15, choices=Status.choices, default=Status.ACTIVE)
    __original_status = Status.ACTIVE

    class Type(models.TextChoices):
        STUDENT = "STUDENT"
        REALTOR = "REALTOR"

    type = models.CharField(max_length=7, choices=Type.choices, default=Type.STUDENT)

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username']

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.__original_status = self.status

    def save(self, *args, **kwargs):
        if self.type == Account.Type.REALTOR and self.status == Account.Status.ACTIVE and \
                self.__original_status != self.status:
            send_email(self.email)

        super().save()
        self.__original_status = self.status

    def __str__(self):
        return self.email


def send_email(email):
    html = get_template('general/account_confirm_template.html')
    d = {}
    html_content = html.render(d)
    subject, from_email, to = 'Подтверждение статуса в Homeet', 'homeet@hse.ru', email
    msg = EmailMessage(subject, html_content, from_email, [to])
    msg.content_subtype = "html"
    msg.send()
