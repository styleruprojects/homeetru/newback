import telegram
from django.conf import settings
from django.template.loader import render_to_string


def post_announcement_on_telegram(announcement, url):
    if announcement.type == "Комната":
        type_url = 'detail-room'
    else:
        type_url = 'detail-flat'
    imageUrl = url + announcement.images.first().image.url
    message_html = render_to_string('telegram_message.html', {
        'announcement': announcement,
        'url': url + '/announcement/' + type_url + '/' + str(announcement.id)
    })
    telegram_settings = settings.TELEGRAM
    bot = telegram.Bot(token=telegram_settings['bot_token'])
    bot.send_photo("@%s" % telegram_settings['channel_name'], imageUrl, message_html,
                   parse_mode=telegram.ParseMode.HTML)
