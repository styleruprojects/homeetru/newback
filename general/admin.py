from django.contrib import admin
from general.models import Account

admin.autodiscover()


class AccountAdmin(admin.ModelAdmin):
    list_display = ('email', 'company_name', 'status', 'type')
    fields = ('email', 'photo', 'phone', 'about', 'first_name', 'last_name', 'realtor_type', 'company_name', 'status',
              'telegram', 'type', 'email_accept_code')
    list_filter = ['status', 'type']
    search_fields = ['email']

    def get_queryset(self, request):
        qs = super(AccountAdmin, self).get_queryset(request)
        return qs.all()


admin.site.register(Account, AccountAdmin)
