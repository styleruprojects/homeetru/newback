from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.forms import AuthenticationForm
from general.models import Account
from django import forms


class AccountRegistrationForm(UserCreationForm):
    iamrealtor = forms.BooleanField(required=False, widget=forms.CheckboxInput(attrs={'class': 'custom-control-input',
                                                                                      'id': 'iamrealtor',
                                                                                      'name': 'iamrealtor'}))

    class Meta:
        model = Account
        fields = ['email', 'type', 'password1', 'password2']

    def __init__(self, *args, **kwargs):
        super(AccountRegistrationForm, self).__init__(*args, **kwargs)
        self.fields['type'].widget = forms.HiddenInput()
        self.fields['email'].widget.attrs.update({'class': 'form-control',
                                                  'placeholder': 'Email'})
        self.fields['password1'].widget.attrs.update({'class': 'form-control', 'placeholder': 'Пароль'})
        self.fields['password2'].widget.attrs.update({'class': 'form-control', 'placeholder': 'Подтверждение пароля'})


class UserLoginForm(AuthenticationForm):

    class Meta:
        model = Account
        fields = ['username', 'password']

    def __init__(self, *args, **kwargs):
        super(UserLoginForm, self).__init__(*args, **kwargs)
        self.error_messages['invalid_login'] = 'Неверный логин или пароль'
        self.fields['username'].widget.attrs.update({'class': 'form-control', 'placeholder': 'Email', 'style': 'text-transform:lowercase;'})
        self.fields['password'].widget.attrs.update({'class': 'form-control', 'placeholder': 'Пароль'})

    def clean_username(self):
        data = self.cleaned_data['username'].lower()
        return data


class RestorePasswordForm(forms.Form):
    password1 = forms.CharField(
        strip=False,
        widget=forms.PasswordInput(attrs={'autocomplete': 'current-password'}),
    )
    password2 = forms.CharField(
        strip=False,
        widget=forms.PasswordInput(attrs={'autocomplete': 'current-password'}),
    )
    restore_key = forms.CharField(
        strip=False,
        widget=forms.HiddenInput(),
    )

    def __init__(self, *args, **kwargs):
        super(RestorePasswordForm, self).__init__(*args, **kwargs)
        self.fields['password1'].widget.attrs.update({'class': 'form-control', 'placeholder': 'Пароль'})
        self.fields['password2'].widget.attrs.update({'class': 'form-control', 'placeholder': 'Подтверждение пароля'})


class EmailForm(forms.Form):
    email = forms.EmailField()

    def __init__(self, *args, **kwargs):
        super(EmailForm, self).__init__(*args, **kwargs)
        self.fields['email'].widget.attrs.update({'class': 'form-control', 'placeholder': 'Email'})
