from django.urls import path, include
from general import views
from django.contrib.auth import views as login_view


urlpatterns = [
    path('', views.to_up, name="up"),
    path('index/', views.index_student, name="index"),
    path('register', views.register_student, name="student-register"),
    path('login', views.my_login, name='login'),
    path('restore', views.restore_password_email, name='restore'),
    path('reset-password', views.reset_password, name='reset-password'),
    path('set-password', views.set_password, name='set-password'),
    path('logout/', login_view.logout_then_login, name='logout'),
    path('photo', views.save_photo, name='student-save-photo'),
    path('activate/<str:code>', views.accept_email, name='accept'),
    path('auth/token', views.token_auth, name="token-auth"),
    path('oidc/', include('mozilla_django_oidc.urls')),
]
