from django.urls import path
from student import views


urlpatterns = [
    path('student-profile/', views.student_profile, name="add-student-profile"),
    path('', views.student, name='student-profile')
]
