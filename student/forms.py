from django.core.validators import RegexValidator
from general.models import Account
from django import forms

CHOICES_FACULTY = (
    ('Факультет математики', 'Факультет математики'),
    ('Факультет экономических наук', 'Факультет экономических наук'),
    ('Московский институт электроники и математики им. А.Н. Тихонова',
     'Московский институт электроники и математики им. А.Н. Тихонова'),
    ('Факультет компьютерных наук', 'Факультет компьютерных наук'),
    ('Факультет права', 'Факультет права'),
    ('Высшая школа бизнеса', 'Высшая школа бизнеса'),
    ('Высшая школа юриспруденции и администрирования', 'Высшая школа юриспруденции и администрирования'),
    ('Факультет гуманитарных наук', 'Факультет гуманитарных наук'),
    ('Факультет социальных наук', 'Факультет социальных наук'),
    ('Факультет социальных наук', 'Факультет социальных наук'),
    ('Факультет коммуникаций, медиа и дизайна', 'Факультет коммуникаций, медиа и дизайна'),
    ('Факультет мировой экономики и мировой политики', 'Факультет мировой экономики и мировой политики'),
    ('Факультет физики', 'Факультет физики'),
    ('Международный институт экономики и финансов', 'Международный институт экономики и финансов'),
    ('Факультет городского и регионального развития', 'Факультет городского и регионального развития'),
    ('Факультет химии', 'Факультет химии'),
    ('Факультет биологии и биотехнологии', 'Факультет биологии и биотехнологии'),
    ('Факультет географии и геоинформационных технологий', 'Факультет географии и геоинформационных технологий'),
    ('Школа иностранных языков', 'Школа иностранных языков'),
    ('Институт статистических исследований и экономики знаний',
     'Институт статистических исследований и экономики знаний'),
    ('Банковский институт', 'Банковский институт'),
)
CHOICES_COURSE = (
    (1, '1'),
    (2, '2'),
    (3, '3'),
    (4, '4'),
    (5, '5'),
    (6, '6')
)


class StudentDataForm(forms.ModelForm):
    course = forms.ChoiceField(choices=CHOICES_COURSE)
    faculty = forms.ChoiceField(choices=CHOICES_FACULTY)
    photo = forms.FileField()
    phone = forms.CharField(
        validators=[RegexValidator(r'^(\+7){1}\s?[\(]*9[0-9]{2}[\)]*\s?\d{3}[-]*\d{2}[-]*\d{2}$')])

    class Meta:
        model = Account
        fields = ['photo', 'first_name', 'last_name', 'phone', 'course', 'faculty', 'about', 'telegram']

    def __init__(self, *args, **kwargs):
        super(StudentDataForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs.update({'class': 'form-control', 'id': 'formGroupExampleInput3'})
        self.fields['last_name'].widget.attrs.update({'class': 'form-control', 'id': 'formGroupExampleInput4'})
        self.fields['phone'].widget.attrs.update(
            {'type': 'tel', 'pattern': '^(\+7){1}\s?[\(]*9[0-9]{2}[\)]*\s?\d{3}[-]*\d{2}[-]*\d{2}$',
             'class': 'form-control', 'id': 'formGroupExampleInput7',
             'title': 'Введите корректный номер, начинающийся с +7',
             'placeholder': '+7...'})
        self.fields['about'].widget.attrs.update(
            {'class': 'form-control', 'id': 'exampleFormControlTextarea1', 'rows': '7'})
        self.fields['course'].widget.attrs.update(
            {'class': 'selectpicker', 'data-live-search': 'true', 'data-width': '100%'})
        self.fields['faculty'].widget.attrs.update(
            {'class': 'selectpicker', 'data-live-search': 'true', 'data-width': '100%'})
        self.fields['photo'].widget.attrs.update({'id': 'upload', 'value': 'Choose a file', 'accept': 'image/*'})
        self.fields['telegram'].widget.attrs.update({'class': 'form-control', 'id': 'formGroupExampleInputTg',
                                                     'placeholder': 'Только username без @'})
