from django.shortcuts import render, redirect, get_object_or_404
from general.models import Account
from student.forms import StudentDataForm
from django.contrib.auth.decorators import login_required
from django.contrib import messages


@login_required
def student_profile(request):
    account = get_object_or_404(klass=Account, pk=request.user.pk)
    if request.method == 'POST':
        form = StudentDataForm(request.POST, instance=account)
        if form.is_valid():
            form.save()
            return redirect('all-announcement')
    else:
        form = StudentDataForm()
    return render(request, 'student/add_student_profile.html', {'form': form})


@login_required
def student(request):
    account = get_object_or_404(klass=Account, pk=request.user.pk)
    if request.method == 'POST':
        form = StudentDataForm(request.POST, instance=account)
        if form.is_valid():
            form.save()
            messages.success(request, "Данные обновлены")
    else:
        form = StudentDataForm(instance=account)
    return render(request, 'student/student_profile.html', {'profile': form})
