from django.contrib import messages
from django.shortcuts import render, get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from realtor.forms import RealtorDataForm
from general.models import Account


@login_required
def realtor_profile(request):
    account = get_object_or_404(klass=Account, pk=request.user.pk)
    if request.method == 'POST':
        form = RealtorDataForm(request.POST, instance=account)
        print(form.is_valid())
        if form.is_valid():
            form.save()
            return redirect('all-announcement')
    else:
        form = RealtorDataForm()
    return render(request, 'realtor/add_realtor_profile.html', {'form': form})


@login_required
def realtor(request):
    account = get_object_or_404(klass=Account, pk=request.user.pk)
    if request.method == 'POST':
        form = RealtorDataForm(request.POST, instance=account)
        if form.is_valid():
            form.save()
            messages.success(request, "Данные обновлены")
    else:
        form = RealtorDataForm(instance=account)
    return render(request, 'realtor/realtor_profile.html', {'profile': form})
