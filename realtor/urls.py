from django.urls import path
from realtor import views
from general import views as general_views


urlpatterns = [
    path('create-realtor-profile/', views.realtor_profile, name="add-realtor-profile"),
    path('index', general_views.index_realtor, name="realtor-index"),
    path('register', general_views.index_realtor, name="realtor-register"),
    path('login', general_views.my_login_realtor, name='realtor-login'),
    path('', views.realtor, name='realtor-profile')
]
