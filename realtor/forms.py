from django.core.validators import RegexValidator
from general.models import Account
from django import forms


STATUS = (
    ('Риелторское агенство', 'Риелторское агенство'),
    ('Частный риелтор', 'Частный риелтор'),
    ('ИП', 'ИП'),
    ('Самозанятый', 'Самозанятый'),
    ('Частное лицо', 'Частное лицо'),
)


class RealtorDataForm(forms.ModelForm):
    realtor_type = forms.ChoiceField(choices=STATUS)
    photo = forms.FileField()
    phone = forms.CharField(validators=[RegexValidator(r'^(\+7){1}\s?[\(]*9[0-9]{2}[\)]*\s?\d{3}[-]*\d{2}[-]*\d{2}$')])

    class Meta:
        model = Account
        fields = ['photo', 'first_name', 'last_name', 'phone', 'realtor_type', 'company_name', 'about']

    def __init__(self, *args, **kwargs):
        super(RealtorDataForm, self).__init__(*args, **kwargs)
        self.fields['first_name'].widget.attrs.update({'class': 'form-control', 'id': 'formGroupExampleInput3'})
        self.fields['last_name'].widget.attrs.update({'class': 'form-control', 'id': 'formGroupExampleInput4'})
        self.fields['phone'].widget.attrs.update({'type': 'tel', 'pattern': '^(\+7){1}\s?[\(]*9[0-9]{2}[\)]*\s?\d{3}[-]*\d{2}[-]*\d{2}$',
                                                  'class': 'form-control', 'id': 'formGroupExampleInput7',
                                                  'title': 'Введите корректный номер, начинающийся с +7',
                                                  'placeholder': '+7...'})
        self.fields['about'].widget.attrs.update(
            {'class': 'form-control', 'id': 'exampleFormControlTextarea1', 'rows': '7'})
        self.fields['realtor_type'].widget.attrs.update(
            {'class': 'selectpicker', 'data-live-search': 'true', 'data-width': '100%'})
        self.fields['company_name'].widget.attrs.update({'class': 'form-control', 'id': 'formGroupExampleInput8'})
        self.fields['photo'].widget.attrs.update({'name': 'image1', 'id': 'image1', 'accept': '.gif, .jpg, .png'})
