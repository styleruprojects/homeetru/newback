from announcement.models import Announcement, Images
from django import forms


ROOMS = (
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
    ('Студия', 'Студия')
)

ROOMS_FILTER = (
    ('initial', 'Количество комнат'),
    ('Студия', 'Студия'),
    ('1', '1'),
    ('2', '2'),
    ('3', '3'),
    ('4', '4'),
)

BEDROOMS = (
    (1, '1'),
    (2, '2'),
    (3, '3'),
    (4, '4'),
)

CHOISE = (
    (True, 'Да'),
    (False, 'Нет')
)

TEMP_REG = (
    ('initial', 'Временная регистрация'),
    (True, 'Да'),
    (False, 'Нет')
)

CHILDREN = (
    ('initial', 'Можно с животными'),
    (True, 'Да'),
    (False, 'Нет')
)

PET = (
    ('initial', 'Можно с детьми'),
    (True, 'Да'),
    (False, 'Нет')
)


class AnnouncementDataForm(forms.ModelForm):
    room_number = forms.ChoiceField(choices=ROOMS)
    bedroom_number = forms.ChoiceField(choices=BEDROOMS)
    has_children = forms.ChoiceField(choices=CHOISE)
    has_pet = forms.ChoiceField(choices=CHOISE)
    temp_registration = forms.ChoiceField(choices=CHOISE)

    class Meta:
        model = Announcement
        fields = ['title', 'deposit', 'description', 'room_number', 'bedroom_number', 'floor', 'price', 'square',
                  'commission', 'has_pet', 'has_children', 'address', 'longitude', 'latitude', 'type', 'temp_registration']

    def __init__(self, *args, **kwargs):
        super(AnnouncementDataForm, self).__init__(*args, **kwargs)
        self.fields['title'].widget.attrs.update({'class': 'form-control', 'id': 'propertyTitle'})
        self.fields['description'].widget.attrs.update({'class': 'form-control', 'id': 'propertyDescription'})
        self.fields['room_number'].widget.attrs.update({'class': 'selectpicker', 'data-live-search': 'true', 'data-width': '100%'})
        self.fields['bedroom_number'].widget.attrs.update({'class': 'selectpicker', 'data-live-search': 'true', 'data-width': '100%'})
        self.fields['floor'].widget.attrs.update({'class': 'form-control', 'id': 'formGroupExampleFloor'})
        self.fields['price'].widget.attrs.update({'class': 'form-control', 'id': 'formGroupExamplePrice'})
        self.fields['square'].widget.attrs.update({'class': 'form-control', 'id': 'formGroupExampleArea'})
        self.fields['deposit'].widget.attrs.update({'class': 'form-control', 'id': 'formGroupExampleDeposit'})
        self.fields['commission'].widget.attrs.update({'class': 'form-control', 'id': 'formGroupExampleDeposit'})
        self.fields['has_children'].widget.attrs.update({'class': 'selectpicker', 'data-live-search': 'true', 'data-width': '100%'})
        self.fields['has_pet'].widget.attrs.update({'class': 'selectpicker', 'data-live-search': 'true', 'data-width': '100%'})
        self.fields['temp_registration'].widget.attrs.update({'class': 'selectpicker', 'data-live-search': 'true', 'data-width': '100%'})
        self.fields['address'].widget.attrs.update({'class': 'form-control', 'id': 'suggest'})
        self.fields['longitude'].widget = forms.HiddenInput(attrs={'id': 'longitude'})
        self.fields['latitude'].widget = forms.HiddenInput(attrs={'id': 'latitude'})
        self.fields['type'].widget = forms.HiddenInput()


class RoomDataForm(forms.ModelForm):
    room_number = forms.ChoiceField(choices=ROOMS)
    has_children = forms.ChoiceField(choices=CHOISE)
    has_pet = forms.ChoiceField(choices=CHOISE)
    temp_registration = forms.ChoiceField(choices=CHOISE)

    class Meta:
        model = Announcement
        fields = ['title', 'deposit', 'description', 'room_number', 'room_square', 'floor', 'price', 'square',
                  'commission', 'has_pet', 'has_children', 'address', 'longitude', 'latitude', 'type', 'temp_registration']

    def __init__(self, *args, **kwargs):
        super(RoomDataForm, self).__init__(*args, **kwargs)
        self.fields['title'].widget.attrs.update({'class': 'form-control', 'id': 'propertyTitle'})
        self.fields['description'].widget.attrs.update({'class': 'form-control', 'id': 'propertyDescription'})
        self.fields['room_number'].widget.attrs.update({'class': 'selectpicker', 'data-live-search': 'true', 'data-width': '100%'})
        self.fields['room_square'].widget.attrs.update({'class': 'form-control', 'id': 'formGroupExampleRoomArea'})
        self.fields['floor'].widget.attrs.update({'class': 'form-control', 'id': 'formGroupExampleFloor'})
        self.fields['price'].widget.attrs.update({'class': 'form-control', 'id': 'formGroupExamplePrice'})
        self.fields['square'].widget.attrs.update({'class': 'form-control', 'id': 'formGroupExampleArea'})
        self.fields['deposit'].widget.attrs.update({'class': 'form-control', 'id': 'formGroupExampleDeposit'})
        self.fields['commission'].widget.attrs.update({'class': 'form-control', 'id': 'formGroupExampleDeposit'})
        self.fields['has_children'].widget.attrs.update({'class': 'selectpicker', 'data-live-search': 'true', 'data-width': '100%'})
        self.fields['has_pet'].widget.attrs.update({'class': 'selectpicker', 'data-live-search': 'true', 'data-width': '100%'})
        self.fields['temp_registration'].widget.attrs.update({'class': 'selectpicker', 'data-live-search': 'true', 'data-width': '100%'})
        self.fields['address'].widget.attrs.update({'class': 'form-control', 'id': 'suggest'})
        self.fields['longitude'].widget = forms.HiddenInput(attrs={'id': 'longitude'})
        self.fields['latitude'].widget = forms.HiddenInput(attrs={'id': 'latitude'})
        self.fields['type'].widget = forms.HiddenInput()


class ImageForm(forms.ModelForm):
    image = forms.ImageField()

    class Meta:
        model = Images
        fields = ('image', )


class SearchForm(forms.Form):
    number_of_rooms = forms.ChoiceField(required=False, choices=ROOMS_FILTER)
    min_price = forms.IntegerField(required=False)
    max_price = forms.IntegerField(required=False)
    temp_registration = forms.ChoiceField(required=False, choices=TEMP_REG)
    with_children = forms.ChoiceField(required=False, choices=CHILDREN)
    with_pet = forms.ChoiceField(required=False, choices=PET)

    def __init__(self, *args, **kwargs):
        super(SearchForm, self).__init__(*args, **kwargs)
        self.fields['number_of_rooms'].widget.attrs.update({'class': 'selectpicker w100 show-tick'})
        self.fields['temp_registration'].widget.attrs.update({'class': 'selectpicker w100 show-tick'})
        self.fields['with_children'].widget.attrs.update({'class': 'selectpicker w100 show-tick'})
        self.fields['with_pet'].widget.attrs.update({'class': 'selectpicker w100 show-tick'})
        self.fields['min_price'].widget.attrs.update({'hidden': True, 'name': 'min-value'})
        self.fields['max_price'].widget.attrs.update({'hidden': True, 'name': 'max-value'})
