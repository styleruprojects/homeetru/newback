from django.urls import path
from announcement import views


urlpatterns = [
    path('all/', views.all_announcement, name="all-announcement"),
    path('all/coordinates', views.all_announcement_coordinates, name="all-announcement-coordinates"),
    path('create/', views.create_announcement, name='create-announcement'),
    path('search/', views.search, name='announcement-search'),
    path('detail-flat/<int:id>', views.detail, name='detail-announcement'),
    path('create-room/', views.create_room, name='create-room'),
    path('detail-room/<int:id>', views.detail_room, name='detail-room'),
    path('my_announcement/', views.my_announcement, name='property'),
    path('switch/', views.switch, name='switch'),
    path('edit-flat/<int:id>', views.edit_flat, name='flat-edit'),
    path('edit-room/<int:id>', views.edit_room, name='room-edit')
]
