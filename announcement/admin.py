from django.contrib import admin
from announcement.models import Announcement


class AnnouncementAdmin(admin.ModelAdmin):
    list_display = ('title', 'author', 'status')
    list_filter = ['status']

    def get_queryset(self, request):
        qs = super(AnnouncementAdmin, self).get_queryset(request)
        return qs.all()


admin.site.register(Announcement, AnnouncementAdmin)
