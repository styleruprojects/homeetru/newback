from django.contrib.auth.decorators import login_required
from django.core.mail import EmailMessage
from django.core.paginator import Paginator
from django.shortcuts import render, redirect, get_object_or_404
from django.template.loader import get_template
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response

from announcement.forms import AnnouncementDataForm, SearchForm, RoomDataForm
from announcement.models import Images, Announcement
from general.models import Account


@login_required
def all_announcement(request):
    account = Account.objects.get(pk=request.user.pk)
    if account.get_full_name() == '':
        if account.type == Account.Type.REALTOR:
            return redirect('add-realtor-profile')
        else:
            return redirect('add-student-profile')
    announcement_filter = {'status': Announcement.Status.ACTIVE, 'is_active': True}
    context = {'search_form': SearchForm()}
    if 'sort' in request.GET:
        if request.GET['sort'] == 'room':
            context['sort'] = 'room'
            announcement_filter['type'] = Announcement.Type.ROOM
        elif request.GET['sort'] == 'flat':
            context['sort'] = 'flat'
            announcement_filter['type'] = Announcement.Type.FLAT
    else:
        context['sort'] = 'all'
    announcements = Announcement.objects.filter(**announcement_filter).order_by('-id')

    paginator = Paginator(announcements, 20)
    page_number = request.GET.get('page')
    context['announcements'] = paginator.get_page(page_number)
    return render(request, 'announcement/flat_list.html', context)


@login_required
def create_announcement(request):
    account = Account.objects.get(pk=request.user.pk)
    if account.type == Account.Type.REALTOR and account.status != Account.Status.ACTIVE:
        return render(request, 'realtor/realtor_alert.html')

    images = request.FILES.getlist('images')
    if request.method == 'POST':
        announcementForm = AnnouncementDataForm(request.POST)
        if len(images) < 7:
            return render(request, 'announcement/create_property.html', {'form': announcementForm, 'error': True})

        if announcementForm.is_valid():
            announcement = announcementForm.save(commit=False)
            announcement.author = request.user
            announcement.save()

            for image in images:
                photo = Images(announcement=announcement, image=image)
                photo.save()

            send_email(account.email, announcement.title)
            return redirect('property')
    else:
        announcementForm = AnnouncementDataForm()
    announcementForm.fields["type"].initial = Announcement.Type.FLAT
    announcementForm.fields["temp_registration"].initial = False
    return render(request, 'announcement/create_property.html', {'form': announcementForm})


@login_required
def create_room(request):
    account = Account.objects.get(pk=request.user.pk)
    if account.type == Account.Type.REALTOR and account.status != Account.Status.ACTIVE:
        return render(request, 'realtor/realtor_alert.html')

    images = request.FILES.getlist('images')
    if request.method == 'POST':
        announcementForm = RoomDataForm(request.POST)
        if len(images) < 7:
            return render(request, 'announcement/create_property_room.html', {'form': announcementForm, 'error': True})

        if announcementForm.is_valid():
            announcement = announcementForm.save(commit=False)
            announcement.author = request.user
            announcement.save()

            for image in images:
                photo = Images(announcement=announcement, image=image)
                photo.save()

            send_email(account.email, announcement.title)
            return redirect('property')
    else:
        announcementForm = RoomDataForm()
    announcementForm.fields["type"].initial = Announcement.Type.ROOM
    announcementForm.fields["temp_registration"].initial = False
    return render(request, 'announcement/create_property_room.html', {'form': announcementForm})


@login_required
def edit_flat(request, id):
    account = Account.objects.get(pk=request.user.pk)
    if account.type == Account.Type.REALTOR and account.status != Account.Status.ACTIVE:
        return render(request, 'realtor/realtor_alert.html')

    announcement = get_object_or_404(Announcement, pk=id)
    if announcement.author != account:
        return render(request, 'announcement/alert.html')

    if request.method == 'POST':
        images = request.FILES.getlist('images')
        announcementForm = AnnouncementDataForm(request.POST, instance=announcement)

        if announcementForm.is_valid():
            announcement = announcementForm.save(commit=False)
            announcement.status = Announcement.Status.MODERATION
            announcement.save()

            for image in images:
                photo = Images(announcement=announcement, image=image)
                photo.save()
            return redirect('all-announcement')
    else:
        announcement = Announcement.objects.get(pk=id)
        announcementForm = AnnouncementDataForm(instance=announcement)
        images = Images.objects.filter(announcement=announcement)
    return render(request, 'announcement/create_property.html', {'form': announcementForm, 'images': images, 'edit': True})


@login_required
def edit_room(request, id):
    account = Account.objects.get(pk=request.user.pk)
    if account.type == Account.Type.REALTOR and account.status != Account.Status.ACTIVE:
        return render(request, 'realtor/realtor_alert.html')

    announcement = get_object_or_404(Announcement, pk=id)
    if announcement.author != account:
        return render(request, 'announcement/alert.html')

    if request.method == 'POST':
        images = request.FILES.getlist('images')
        announcementForm = RoomDataForm(request.POST, instance=announcement)

        if announcementForm.is_valid():
            announcement = announcementForm.save(commit=False)
            announcement.status = Announcement.Status.MODERATION
            announcement.save()

            for image in images:
                photo = Images(announcement=announcement, image=image)
                photo.save()
            return redirect('all-announcement')
    else:
        announcement = Announcement.objects.get(pk=id)
        announcementForm = RoomDataForm(instance=announcement)
        images = Images.objects.filter(announcement=announcement)
    return render(request, 'announcement/create_property_room.html', {'form': announcementForm, 'images': images, 'edit': True})


@login_required
def detail(request, id):
    announcement = get_object_or_404(Announcement, pk=id)
    account = Account.objects.get(pk=request.user.pk)

    if announcement.status == Announcement.Status.MODERATION and not account.is_superuser:
        return render(request, 'announcement/alert.html')

    length = len(announcement.images.all())
    diff = length - 7
    return render(request, 'announcement/flat_item.html', {'announcement': announcement, 'range1': range(1, 7),
                                                           'range2': range(7, length), 'diff': diff})


@login_required
def detail_room(request, id):
    announcement = get_object_or_404(Announcement, pk=id)
    account = Account.objects.get(pk=request.user.pk)

    if announcement.status == Announcement.Status.MODERATION and not account.is_superuser:
        return render(request, 'announcement/alert.html')

    length = len(announcement.images.all())
    diff = length - 7
    return render(request, 'announcement/room_item.html', {'announcement': announcement, 'range1': range(1, 7),
                                                           'range2': range(7, length), 'diff': diff})


@login_required
def my_announcement(request):
    announcements = Announcement.objects.filter(author=request.user).order_by('id')
    paginator = Paginator(announcements, 6)
    page_number = request.GET.get('page')
    return render(request, 'announcement/ap_property.html', {'announcements': paginator.get_page(page_number)})


@login_required
def search(request):
    account = Account.objects.get(pk=request.user.pk)
    if account.get_full_name() == '':
        if account.type == Account.Type.REALTOR:
            return redirect('add-realtor-profile')
        else:
            return redirect('add-student-profile')
    announcements_filter = {'status': Announcement.Status.ACTIVE, 'is_active': True}
    form = SearchForm()
    sort = 'all'
    if request.method == 'POST':
        form = SearchForm(request.POST)
        form_min_price = int(form['min_price'].value())
        form_max_price = int(form['max_price'].value())
        number_of_rooms = form['number_of_rooms'].value()
        temp_registration = form['temp_registration'].value()
        with_children = form['with_children'].value()
        with_pet = form['with_pet'].value()

        announcements_filter = {'status': Announcement.Status.ACTIVE,
                                'is_active': True,
                                'price__gte': form_min_price,
                                'price__lte': form_max_price}

        if temp_registration != 'initial':
            announcements_filter['temp_registration'] = temp_registration

        if number_of_rooms != 'initial':
            announcements_filter['room_number'] = number_of_rooms

        if with_children != 'initial':
            announcements_filter['has_children'] = with_children

        if with_pet != 'initial':
            announcements_filter['has_pet'] = with_pet

        if 'sort' in request.GET:
            if request.GET['sort'] == 'room':
                sort = 'room'
                announcements_filter['type'] = Announcement.Type.ROOM
            elif request.GET['sort'] == 'flat':
                sort = 'flat'
                announcements_filter['type'] = Announcement.Type.FLAT
    announcements = Announcement.objects.filter(**announcements_filter).order_by('-id')
    paginator = Paginator(announcements, 20)
    page_number = request.GET.get('page')
    return render(request, 'announcement/flat_list.html', {'announcements': paginator.get_page(page_number),
                                                           'search_form': form, 'sort': sort})


def switch(request):
    id = request.GET.get('id')
    account = Account.objects.get(pk=request.user.pk)
    announcement = Announcement.objects.get(pk=id)
    if announcement.author != account:
        return render(request, 'announcement/alert.html')

    if announcement.is_active:
        announcement.is_active = False
        announcement.status = Announcement.Status.DISABLED
    else:
        announcement.is_active = True
        announcement.status = Announcement.Status.MODERATION
    announcement.save(update_fields=['is_active', 'status'])
    return my_announcement(request)


@api_view(['GET'])
def all_announcement_coordinates(request):
    announcement_type = request.GET.get('type', 'all')
    if announcement_type == 'flat':
        announcements = Announcement.objects.filter(status=Announcement.Status.ACTIVE, is_active=True,
                                                    type=Announcement.Type.FLAT)
    elif announcement_type == 'room':
        announcements = Announcement.objects.filter(status=Announcement.Status.ACTIVE, is_active=True,
                                                    type=Announcement.Type.ROOM)
    else:
        announcements = Announcement.objects.filter(status=Announcement.Status.ACTIVE, is_active=True)
    data = []
    for announcement in announcements:
        data.append({
            'title': announcement.title,
            'longitude': announcement.longitude,
            'latitude': announcement.latitude,
            'image': announcement.images.first().image.url,
            'price': announcement.price,
            'href': 'http://' + str(request.get_host()) + '/announcement/detail-flat/' + str(announcement.pk)
        })
    return Response(status=status.HTTP_200_OK, content_type="application/json", data=data)


def send_email(email, title):
    html = get_template('general/announcement_new_template.html')
    d = {'title': title}
    html_content = html.render(d)
    subject, from_email, to = 'Создание объявления в Homeet', 'homeet@hse.ru', email
    msg = EmailMessage(subject, html_content, from_email, [to])
    msg.content_subtype = "html"
    msg.send()
