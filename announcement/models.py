from django.core.mail import EmailMessage
from django.db import models
from django.template.loader import get_template
from django.urls import reverse

from general.bot import post_announcement_on_telegram
from general.models import Account


def get_flat_image(self, name):
    return f'announcement_images/{self.announcement.pk}/{name}'


class Announcement(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    room_number = models.CharField(max_length=6)
    bedroom_number = models.IntegerField(default=1)
    room_square = models.FloatField(default=0)
    floor = models.IntegerField()
    price = models.IntegerField()
    square = models.FloatField()
    deposit = models.IntegerField()
    commission = models.IntegerField()
    has_children = models.BooleanField()
    has_pet = models.BooleanField()
    address = models.CharField(max_length=255)
    author = models.ForeignKey(Account, on_delete=models.CASCADE, null=True)
    is_active = models.BooleanField(default=True)
    longitude = models.CharField(max_length=100, null=True)
    latitude = models.CharField(max_length=100, null=True)
    temp_registration = models.BooleanField(default=False, null=True)

    class Status(models.TextChoices):
        ACTIVE = "Активно"
        MODERATION = "На модерации"
        DISABLED = "Приостановлено"

    status = models.CharField(max_length=30, choices=Status.choices, default=Status.MODERATION)

    class Type(models.TextChoices):
        ROOM = "Комната"
        FLAT = "Квартира"

    type = models.CharField(max_length=30, choices=Type.choices, default=Type.FLAT)

    def get_absolute_url(self):
        if self.type == Announcement.Type.FLAT:
            return reverse('detail-announcement', args=[str(self.id)])
        return reverse('detail-room', args=[str(self.id)])

    def __str__(self):
        return self.title

    def get_author(self):
        return Account.objects.get(pk=self.author_id)

    def save(self, *args, **kwargs):
        super(Announcement, self).save(*args, **kwargs)

        if self.status == Announcement.Status.ACTIVE:
            send_email(self.author.email, self.title)
            post_announcement_on_telegram(self, 'https://homeet.hse.ru')


class Images(models.Model):
    announcement = models.ForeignKey(Announcement, on_delete=models.CASCADE, null=True, default=None,
                                     related_name='images')
    image = models.ImageField(upload_to=get_flat_image)


def send_email(email, title):
    html = get_template('general/announcement_confirm_template.html')
    d = {'title': title}
    html_content = html.render(d)
    subject, from_email, to = 'Обновление статуса объявления в Homeet', 'homeet@hse.ru', email
    msg = EmailMessage(subject, html_content, from_email, [to])
    msg.content_subtype = "html"
    msg.send()
