from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('general.urls'), name='general'),
    path('announcement/', include('announcement.urls'), name='announcement'),
    path('student/', include('student.urls'), name='student'),
    path('realtor/', include('realtor.urls'), name='realtor'),
    # path('captcha/', include('captcha.urls')),
]

if settings.DEBUG:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
